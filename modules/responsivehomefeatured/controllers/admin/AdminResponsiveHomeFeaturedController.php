<?php

/**
 * Class AdminResponsiveHomeFeaturedController
 *
 * @author Thomas Peigné <thomas.peigne@gmail.com>
 */
class AdminResponsiveHomeFeaturedController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = false;
        $this->display = 'view';
        $this->meta_title = $this->l('Responsive products featured');
        parent::__construct();
        if (!$this->module->active)
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
    }

    public function renderView()
    {

        return parent::renderView();
    }
} 